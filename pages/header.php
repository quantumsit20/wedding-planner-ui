<!DOCTYPE html>


<html direction="rtl" dir="rtl" style="direction: rtl">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <?php session_start(); ?>
	<title><?php if(isset($_SESSION['title'])) echo $_SESSION['title']; else echo 'dashboard'; ?></title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

	<!--begin::Web font -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
			active: function () {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!--datatable links -->
	<link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/demo/custom/components/mydtable/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="assets/demo/custom/components/mydtable/dataTables.jqueryui.min.css">
	<link rel="stylesheet" type="text/css" href="assets/demo/custom/components/mydtable/dataTables.bootstrap4.css">
	<link rel="stylesheet" type="text/css" href="assets/demo/custom/components/mydtable/buttons.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="assets/demo/custom/components/mydtable/datatables.min.css">

    <!------------------------------------------------------------------------------------------------------------------>
	<!--begin:: Global Mandatory Vendors -->
	<link href="vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>

	<!--end:: Global Mandatory Vendors -->

	<!--begin:: Global Optional Vendors -->
	<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css"/>
	<link rel="shortcut icon" href="assets/demo/media/img/logo/favicon.icon"/>
	<link href="vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/animate.css/animate.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css"/>
	<link href="vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css"/>

	<!--end:: Global Optional Vendors -->

	<!--begin::Global Theme Styles -->
	 <link href="assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />

	<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css"/>

	<link href="vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />


	<!--end::Global Theme Styles -->

	<!--begin::Page Vendors Styles -->
	<!-- <link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" /> -->

	<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css"/>

	<!--end::Page Vendors Styles -->
	<link rel="shortcut icon" href="assets/demo/media/img/logo/favicon.ico"/>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">