<!-- end::Messages Sidebar -->


<!--begin:: Global Mandatory Vendors -->
<script src="vendors/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="vendors/moment/min/moment.min.js" type="text/javascript"></script>
<script src="vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>

<!--end:: Global Mandatory Vendors -->

<!--begin:: Global Optional Vendors -->
<script src="vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
<script src="vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
<script src="vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js"
		type="text/javascript"></script>
<script src="vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js"
		type="text/javascript"></script>
<script src="vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js"
		type="text/javascript"></script>
<script src="vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
<script src="vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
<script src="vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
<script src="vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js"
		type="text/javascript"></script>
<script src="vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
<script src="vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
<script src="vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
<script src="vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
<script src="vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
<script src="vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
<script src="vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
<script src="vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
<script src="vendors/autosize/dist/autosize.js" type="text/javascript"></script>
<script src="vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
<script src="vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
<script src="vendors/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="vendors/markdown/lib/markdown.js" type="text/javascript"></script>
<script src="vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
<script src="vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<script src="vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
<script src="vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
<script src="vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
<script src="vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="vendors/jstree/dist/jstree.js" type="text/javascript"></script>
<script src="vendors/raphael/raphael.js" type="text/javascript"></script>
<script src="vendors/morris.js/morris.js" type="text/javascript"></script>
<script src="vendors/chartist/dist/chartist.js" type="text/javascript"></script>
<script src="vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
<script src="vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
<script src="vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"
		type="text/javascript"></script>
<script src="vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
<script src="vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
<script src="vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
<script src="vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
<script src="vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

<!--end:: Global Optional Vendors -->
<!--begin::Global Theme Bundle -->
<script src="assets/demo/base/scripts.bundle.js" type="text/javascript"></script>
<script src="assets/vendorscustom/jquery-ui/jquery-ui.bundle.rtl.min.css" type="text/javascript"></script>


<!--begin::Page Vendors -->

<!--end::Page Vendors -->

<!--begin::Page Scripts -->

<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="assets/demo/custom/components/mydtable/jquery.dataTables.js"></script>
<script src="assets/demo/custom/components/mydtable/dataTables.jqueryui.min.js"></script>
<script src="assets/demo/custom/components/mydtable/dataTables.bootstrap4.min.js"></script>
<script src="assets/demo/custom/components/mydtable/datatables.min.js"></script>
<script>

	$(document).ready(function () {
		$('#table_id').DataTable();
		$('.dataTables_length').addClass('bs-select');

	});

	$(".btnDelete").click(function(){
		var UserID = $(this).parent().parent().attr("userID");

		$("#confirm").attr("UserID",UserID);

	});

	$("#confirm").click(function(){
		var UserID = $(this).attr("UserID");
		$.ajax({
			type: "POST",
			url: url,
			data:{ID : UserID},
			success: function (response) {
				alert("done");
			},
			error: () => {

				alert("ERROR");
		}

		});
	});
</script>

<!--end::Global Theme Bundle -->
</body>

<!-- end::Body -->
</html>